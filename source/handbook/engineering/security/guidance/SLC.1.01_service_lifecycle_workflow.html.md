---
layout: handbook-page-toc
title: "SLC.1.01 - Service Lifecycle Workflow Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SLC.1.01 - Service Lifecycle Workflow

## Control Statement

Major software releases go through all phases of the Service Life Cycle: Plan, Create, Verify, Package, Secure, Release, Configure, Monitor

## Context

The purpose of this control is to formalize the documentation and approval of software changes before those changes are implemented. This rigid process helps protect GitLab from insecure code being quickly pushed out into production without proper vetting.

## Scope

This control applies to all major software releases to GitLab.com.

## Ownership

* Control Owner: `Delivery`
* Process owner(s):
    * Delivery
    * Infrastructure

## Guidance

Most of this process is already captured in current GitLab workflow; the difficult part of this process will be coverage of all major software changes.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Service Lifecycle Workflow control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/888).

Examples of evidence an auditor might request to satisfy this control:

* Documentation outlining our development and release workflows
* Sample releases and their respective reviews
* Sample pipeline artifacts and merge request reviews
* Feature requests would be a great example of this process since they are planned via epics and issues, created via MR's, and then run through a GitLab pipeline to satisfy the remaining requirements for this control

### Policy Reference

## Framework Mapping

* ISO
  * A.14.1.1
  * A.14.2.5
* SOC2 CC
  * CC8.1
* PCI
  * 6.3
