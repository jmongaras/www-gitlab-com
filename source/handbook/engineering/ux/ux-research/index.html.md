---
layout: handbook-page-toc
title: "UX Research"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## UX Research at GitLab

The goal of [UX Research at GitLab](https://about.gitlab.com/blog/2017/12/20/conducting-remote-ux-research/) is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab. We use these insights to inform and strengthen product and design decisions.


### GitLab First Look
GitLab First Look (formerly the UX Research Panel) is a group of users who have opted in to receive research studies from GitLab. The UX Research team manages and maintains this program. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html).


### UX Researcher workflow
If you're a UX Researcher who is new to GitLab, check out [this page][ux-researcher] for information that's specific to your role.


#### UX Research label
Both the [GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE project](https://gitlab.com/gitlab-org/gitlab-ee) contain a `UX Research` label. The purpose of this label is to help Product Designers and Product Managers keep track of issues which they feel may need UX Research support in the future or which are currently undergoing UX Research. 

UX Researchers are not responsible for maintaining the `UX Research` label. The `UX Research` label should not be used to request research from UX Researchers. Instead, please follow the process outlined in [How to request research](/handbook/engineering/ux/ux-research/#how-to-request-research).

Learn more about how we use Workflow labels in the [GitLab Docs](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html).

#### How to request research

1.  For usability testing, user interviews, card sorts, surveys, or if you are not sure what form of research needs to take place, create a new issue using the `Research proposal` [template](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Research%20proposal.md) in the [UX research project](https://gitlab.com/gitlab-org/ux-research).    

    For beta testing, create a new issue using the `Beta testing proposal` [template](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Beta%20testing%20proposal.md) in the [UX research project](https://gitlab.com/gitlab-org/ux-research).

1. `@` mention the relevant UX Researcher, Product Designer, UX Manager, and Product Manager for the [product stage](/handbook/product/categories/#devops-stages). Ensure you answer all questions outlined in the template.

    * You can find out who the relevant UX Researcher and/or Product Designer is by looking at the [team page](/company/team/) and filtering by the `UX` department.

    * Anybody across GitLab can raise a proposal. This includes UX Researchers.

1. The UX Researcher will review the issue and may respond with some follow-up questions.

    * We want to lessen the time that researchers spend within issues and/or Slack soliciting research requirements. As a rule of thumb, if we have gone back and forth more than 3 times, it's time for a video call.

    * Similarly, some features are more complex than others. A UX Researcher needs to fully understand the feature they are testing and why they are testing it. Sometimes, it's much easier to get a grasp on the feature's history, your existing plans and plans for the future when you talk with us. In cases such as these, the UX Researcher will schedule a kick-off call with all relevant stakeholders (including the Product Designer, UX Manager, and Product Manager) to run through the research proposal.

1.  In collaboration with the Product Manager and UX Manager, the UX Researcher will determine the priority of the study and schedule it accordingly.

1.  The UX Researcher will book a wash-up meeting with all relevant stakeholders when the results of the study are available.

### Training resources

UX Researchers aren't the only GitLabbers who conduct user research. Other roles, like Product Managers and Product Designers, frequently conduct research, too, with guidance from the UX Research team. If you need to conduct a user research study, please refer to the [UX Research Training](/handbook/engineering/ux/ux-researcher-training/) resources to help get you started.


### Conducting research as a Product Manager or Product Designer
As part of the validation track of [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/#validation-track), Product Managers and Product Designers are encouraged to conduct both problem and solution validation research studies.

Here are the steps that may be involved in a research study from this perspective:
1. Product Manager (PM) or Product Designer (PD) opens an issue in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/issues/new#).
1. Product Manager fills out an [Opportunity Canvas](https://docs.google.com/document/d/1pTEMcwH10xWilQEnVc65oC6PdC3VMjn2XoARfNTaHkc/edit#) in collaboration with design, research, and engineering.
1. PM, design, and research meet to discuss the Opportunity Canvas and determine the research hypothesis, goals, and objectives.
1. A [Discussion Guide](https://drive.google.com/open?id=1ERpTsQs7vcKKHLFZ5qoTukUFFA1sdazaFzknsX0Ju5Q) can be created at this point to include the hypothesis, goals, objectives, and questions you want to know the answers to. Refinement and iteration commence between PM, research, and design.
1. All stakeholders work together to identify the users who need to be represented in the research. This information is added to the [Discussion Guide](https://drive.google.com/open?id=1ERpTsQs7vcKKHLFZ5qoTukUFFA1sdazaFzknsX0Ju5Q).
1. When all the necessary information is outlined, a screener survey can then be created in Qualtrics (if you don't have access, [open a request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/)).
1. After the screener survey is created, the PM/PD should inform the Research Coordinator of the recruitment needs by creating a [recruiting request issue](https://gitlab.com/gitlab-org/ux-research/issues/new#). The Research Coordinator will send the screener out to users, according to the criteria from the screener. They will work with the PM to ensure respondents have the right times/days to choose from in Calendly.
1. As participants start to schedule time slots, the PM or PD then adds the sessions to the shared "UX Research" calendar so others can participate (i.e. take notes).
1. The PM or PD conducts research sessions and takes notes ([sample template](https://docs.google.com/spreadsheets/d/1HSNqaPbT3r0m_tl5VopvO2HWoqdIRysbcmFxc5AVQtk/edit?usp=sharing)) along with others as applicable.
1. After the sessions are concluded, PM, design, and research work to synthesize the data and identify trends, resulting in findings.
1. The PM or PD leading the research study should ensure that all participants are compensated by creating an [incentive request](https://gitlab.com/gitlab-org/ux-research/issues/new#) and assigning it to the Research Coordinator.
1. PM or PD then creates an issue in the [UXR_Insights repository](https://gitlab.com/gitlab-org/uxr_insights/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) to collect the videos and high level notes about the project. Each subsequent finding is created as an issue and linked to that main issue. See example of an insights epic [here](https://gitlab.com/gitlab-org/uxr_insights/issues/684). Click here for more [information](https://medium.com/@tsharon/the-atomic-unit-of-a-research-insight-7bf13ec8fabe) on how we create our [insights](https://medium.com/@tsharon/foundations-of-atomic-research-a937d5da5fbb).
1. After all findings have been documented, the PM or PD leading the study should share insights with all applicable stakeholders (research, design, development, etc.) and close the issues associated with the study.


### UXR_Insights Repository

The [UXR_Insights repository](https://gitlab.com/gitlab-org/uxr_insights) is the single source of truth (SSOT) for all user insights discovered by GitLab’s UX Researchers, Product Designers, and Product Managers. 

Instead of reports and slide decks, we use issues to document key findings from research studies. Every issue within the UXR_Insights repository contains a single insight on a particular topic. Each insight is supported with evidence, typically in the form of a video clip or statistical data. We use GitLab’s related issues functionality to connect insights which were revealed during the same research study. We close issues when we have finished editing them.

We use labels to tag insights. They allow us to quickly and dynamically search and filter through issues to find the insights we need. As a rule of thumb, we generally tag labels with the relevant stage group (`devops::plan`, `Secure`, etc), [research type](https://gitlab.com/gitlab-org/uxr_insights/tree/master#research-types) and the area/feature (navigation, merge requests, etc) of GitLab the insight relates to as a bare minimum.

We use Epics to track issues from the same research study. The Epic description usually contains the research methodology used, and if we spoke with users, then we’ll include any background information we have about them. We tag all Epics with the `uxr_insights` label so they are easily found within the GitLab.org group.

A directory of completed research is available in the repository's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) file.

### How to request recruiting and scheduling support

UX researchers, product managers, and designers are welcome to request support with recruiting for their studies. 

1. Please open an issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/tree/master/.gitlab/issue_templates) to request recruiting help once you’ve gained a clear idea of your goals and who you want to speak with, and drafted a screener. Please use the template called `recruiting request` and respond to the prompts with your details.

1. The research coordinator or UX researcher will do a check to make sure your screener will catch the people you’ve identified as your target participants. There may be some back and forth, but the goal is to have your criteria finalized before drafting the screener and requesting recruiting help. If there are multiple rounds of review, the coordinator will pause activities until uncertainty about your screening criteria has been resolved. 

#### Recruitment methods

1. GitLab First Look - our research participant panel. This is a good fit for studies that are aimed at GitLab users who are software developers and related roles. The panel currently does not have many security professionals, nor senior engineering leaders. The coordinator will launch and monitor campaigns in Qualtrics to invite First Look panelists.

1. Respondent - a recruitment service that is a good choice for studies aimed at software professionals who are not necessarily GitLab users. This has been a decent source of security professionals and some other harder-to-reach users. The coordinator will launch and monitor campaigns in Respondent.

1. Social outreach - social posts may go out on GitLab's brand channels. The coordinator requests these using the social request template in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing). This is a good choice for studies primarily aimed at GitLab users. Product managers and other teammates are highly encouraged to help promote their studies to their networks. 

#### Scheduling interviews 

The coordinator will open [an issue](https://gitlab.com/gitlab-org/ux-research/tree/master) to schedule users with the interviewer; there is a template called `scheduling-request` that has some steps for the interviewer to configure their Calendly. 

#### Sending thank you gifts

A typical thank you gift for research participants is a $60 Amazon gift card, or a GitLab swag item. The coordinator will arrange for delivery. Please open an issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/tree/master) and use the template `incentives-request` to ask for a gift to be sent to interviewees.

### Beta Testing at GitLab

#### Aims

* Collect quotes from users that we can use in release posts.
* Generate feature awareness.
* Identify bugs and/or improvements for a feature.
* Gather general user feedback.


#### FAQs about Beta Testing

1.  How can I request beta testing?

    You can request beta testing by following the instructions outlined in ['How to request research'](/handbook/engineering/ux/ux-research/#how-to-request-research).

1.  How do users see a feature in beta?

    First iteration: Test feature(s) as they are added to a [release candidate](https://gitlab.com/gitlab-org/release/docs/blob/master/general/release-candidates.md).

    Future iterations: Route beta users to a Canary version of GitLab that runs pre-release code. Use feature flags.

1.  How do users provide feedback about a feature?

    By completing a survey.

1.  What if I don't know what release candidate the feature will be added in?

    That's okay. All survey questions should be finalized in advance of the first release candidate, so that the UX Researcher can build the survey and the accompanying mail campaign ahead of time.

    When the feature is added to a release candidate, ping the relevant UX Researcher in the issue you have created. The UX Researcher will then distribute the survey to users.

1.  What if the feature is added to a late release candidate? Is there any point in still beta testing the feature?

    Yes, you can still beta test the feature.

    The overall aims of your study will be to:
    
    * Generate feature awareness.
    * Identify bugs and/or improvements for a feature.
    * Gather general user feedback.

    It may be beneficial to delay sending the survey until the official release on the 22nd to save users the effort of downloading a release candidate.

1.  Can I request beta testing for a feature that isn't labeled as a [beta](/handbook/product/#alpha-beta-ga) release?

    Yes, you can test any feature in an upcoming release candidate/milestone.

1.  How long will a feature be tested for?

    This depends on which release candidate the feature is added to and what the ultimate aims of the study are.

    If the feature is added to an early release candidate. Your main aim might be to:

    * Collect quotes from users that we can use in release posts.

    In which case, there's not much point running the survey past the 22nd of the month.

    If the feature is added to a later release candidate. Your main aims might be to:

    * Generate feature awareness.
    * Identify bugs and/or improvements for a feature.
    * Gather general user feedback.

    Therefore, we recommend running the survey until it stops receiving responses.

1.  What are some example questions that I could ask users?

    * How could the feature be improved? (Open text)
    * What, if anything, didn’t work as you expected it to? (Open text)
    * Does this feature help you accomplish X? (Rating scale)
        * Please explain your answer (Open text)
    * What triggers would prompt you to use this feature? (Open text)
    * How likely is it that you would use this feature? (Rating scale)
        * Why is it unlikely that would use this feature? (Open text)
    * What do you most like about the feature? (Open text)
    * Overall, how easy was it to use the feature? (Rating scale)


[ux-researcher]: /handbook/engineering/ux/ux-researcher/
[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
